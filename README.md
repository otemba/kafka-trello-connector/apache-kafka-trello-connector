# Apache-Kafka-Trello-Connector


A simple Trello connector that mirrors Boards of a team in a source and a sink Topic. 

It uses the [Trello API](https://developers.trello.com/docs/api-introduction) to source or sink the Boards, Cards and Comments of a Trello team. 

Source means that you can specify a Topic in which the elements are entered as jsons by their IDs as keys. When starting the connector performs a one-time-delivery of all items. When users make changes in Trello, only the changed items are delivered to the source Topic again.

Sink means that you can create, update or delete items for the Trello team. This is done by feeding jsons to the sink Topic. The connector picks them up to make changes in Trello. 

## Motivation

In some rare cases an organization needs to track changes in Trello Boards. 

Personally I experienced that Trello boards can be used as __user-friendly representations of a system state__. For example, you can use it to __control__ or __monitor__ a KStream. You may be wondering why I should do that if I have the CCC, CLI and DSL at hand? Well, maybe some, but not all have that. In addition, Trello integrates __notifications__. 

You can view alerts as a free bonus for users of the Trello connector for Kafka because users can control themselves what needs to be notified and what does not. Another advantage is responsiveness. __Trello is available for the users of any device.__

I hope you will find that everything fits together well as I do.

With some creativity, there are other use cases that are still unknown. I am looking forward to your ideas. Please share.

## Security

The Trello connector uses the security model of Trello and its API.

## Future development

I am working on a test loop for demonstration purposes, where one Trello board follows the changes of another. 

## Download

You may want to download as a zip and unpack. As a result you will find all you need in the directory "trello-connector":
* connect-standalone.properties (in case you want to run this connector in stand alone mode)
* trello-source.properties (sample properties)
* trello-connector-0.0.1-jar-with-dependencies.jar (the actual connector)
* trello-sink.properties (sample properties)

## Prepare your properties

You will find incomplete entries and their Comments in the property files. Please update accordingly. Then move the file collection above to your Kafka plugin.path. 

## Prepare your running Kafka instance

Create your Topics and be aware that they must match with the entries in the property files:

```
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic trello-raw-json-by-id
```

```
kafka-topics --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic trello-json-rest-requests
```

## Experiment by starting the source connector and inspect the results

```
kafka-console-consumer --bootstrap-server localhost:9092 --topic trello-raw-json-by-id --from-beginning
```

```
connect-standalone connect-standalone.properties trello-source.properties
```

## Start the sink connector and watch the results in Trello

```
kafka-console-producer --bootstrap-server localhost:9092 --topic trello-json-rest-requests
```
```
connect-standalone connect-standalone.properties trello-sink.properties
```
### Sample REST requests to create, update and delete Cards of a Board in Trello
Put the requests in the sample sink Topic __trello-json-rest-requests__ to create, update or delete Boards, Cards or Comments in your Trello account. The next sample request shows that you need ids from Trello. You may extract those from the samples source Topic __trello-raw-json-by-id__.

```
{
	"type": "card",
	"request": "create",
	"details": {
		"idBoard": "5cee979ab37a3b1c033f4f0b",
		"idList": "5cee97a11881f54ff46af229",
		"title": "first card created by the Kafka-Connector",
		"desc": "If you start the connector then Trello is close at hand"
	}
}
```
You also may find the id of your Board and its lists by opening your Board in Trello and navigating to Show Menu -> ... More -> Print and Export -> Export as JSON. That is widely accepted as more convenient for experiments. 

```
{"id":"5cee979ab37a3b1c033f4f0b","name":"kafka-connect-....",
... {"list":{"name":"ToDo","id":"5cee97a11881f54ff46af229"}} 
```
```
{
	"type": "card",
	"request": "update",
	"id": "CARD-ID",
	"details": {
		"title": "updated by testcase",
		"desc": "description updated by testcase"
	}
}
```
Get the id for the Card: Same as above.

```
{
	"type": "card",
	"request": "delete",
	"id": "CARD-ID"
}
```

Get the id for the Card: Same as above.
 